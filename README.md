Version-1.2.0

## Package **resume-db-persistence**

Data models and migrations based on Sequelize ORM to create all necessary database objects.

The package includes docker environment. The environment provides the following services:
* MariaDB - relational database;
* Migrations - service including node.js to run migrations;
* phpMyAdmin - tool for quering database.

**Before running any command shown below please install dependencies first.**

#### To install package dependencies:

```
docker run --rm -u `id -u` -v $(pwd):/app -w /app node:8.9 npm install
```

#### To run docker environment:

```
docker-compose up -d
```
After database is run the migrations will be executed automatically. If for some reason it didn't happen the migrations service can be run manualy (see command below).

#### To check docker services status:

```
docker-compose ps
```

#### To run migrations manualy:

```
docker-compose run --rm migrations ./node_modules/.bin/sequelize db:migrate
```

#### To undo latest migration:

```
docker-compose run --rm migrations ./node_modules/.bin/sequelize db:migrate:undo
```

#### To undo all run migration:

```
docker-compose run --rm migrations ./node_modules/.bin/sequelize db:migrate:undo:all
```

For extra info on available commands and options please refer to [Sequelize CLI](http://docs.sequelizejs.com/manual/tutorial/migrations.html) official documentation.

#### How to access docker services:

* MariaDB database - localhost:8506 (username: root, password: rand0m!)
* phpMyAdmin - [http://localhost:8580/](http://localhost:8580/)
'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Languages', [
      {
        id: 1,
        name: 'Abkhazian'
      },
      {
        id: 2,
        name: 'Afar'
      },
      {
        id: 3,
        name: 'Afrikaans'
      },
      {
        id: 4,
        name: 'Akan'
      },
      {
        id: 5,
        name: 'Albanian'
      },
      {
        id: 6,
        name: 'Amharic'
      },
      {
        id: 7,
        name: 'Arabic'
      },
      {
        id: 8,
        name: 'Aragonese'
      },
      {
        id: 9,
        name: 'Armenian'
      },
      {
        id: 10,
        name: 'Assamese'
      },
      {
        id: 11,
        name: 'Avaric'
      },
      {
        id: 12,
        name: 'Avestan'
      },
      {
        id: 13,
        name: 'Aymara'
      },
      {
        id: 14,
        name: 'Azerbaijani'
      },
      {
        id: 15,
        name: 'Bambara'
      },
      {
        id: 16,
        name: 'Bashkir'
      },
      {
        id: 17,
        name: 'Basque'
      },
      {
        id: 18,
        name: 'Belarusian'
      },
      {
        id: 19,
        name: 'Bengali'
      },
      {
        id: 20,
        name: 'Bihari languages'
      },
      {
        id: 21,
        name: 'Bislama'
      },
      {
        id: 22,
        name: 'Bosnian'
      },
      {
        id: 23,
        name: 'Breton'
      },
      {
        id: 24,
        name: 'Bulgarian'
      },
      {
        id: 25,
        name: 'Burmese'
      },
      {
        id: 26,
        name: 'Catalan'
      },
      {
        id: 27,
        name: 'Chamorro'
      },
      {
        id: 28,
        name: 'Chechen'
      },
      {
        id: 29,
        name: 'Chichewa'
      },
      {
        id: 30,
        name: 'Chinese'
      },
      {
        id: 31,
        name: 'Chuvash'
      },
      {
        id: 32,
        name: 'Cornish'
      },
      {
        id: 33,
        name: 'Corsican'
      },
      {
        id: 34,
        name: 'Cree'
      },
      {
        id: 35,
        name: 'Croatian'
      },
      {
        id: 36,
        name: 'Czech'
      },
      {
        id: 37,
        name: 'Danish'
      },
      {
        id: 38,
        name: 'Divehi'
      },
      {
        id: 39,
        name: 'Dutch'
      },
      {
        id: 40,
        name: 'Dzongkha'
      },
      {
        id: 41,
        name: 'English'
      },
      {
        id: 42,
        name: 'Esperanto'
      },
      {
        id: 43,
        name: 'Estonian'
      },
      {
        id: 44,
        name: 'Ewe'
      },
      {
        id: 45,
        name: 'Faroese'
      },
      {
        id: 46,
        name: 'Fijian'
      },
      {
        id: 47,
        name: 'Finnish'
      },
      {
        id: 48,
        name: 'French'
      },
      {
        id: 49,
        name: 'Fulah'
      },
      {
        id: 50,
        name: 'Galician'
      },
      {
        id: 51,
        name: 'Georgian'
      },
      {
        id: 52,
        name: 'German'
      },
      {
        id: 53,
        name: 'Greek'
      },
      {
        id: 54,
        name: 'Guaraní'
      },
      {
        id: 55,
        name: 'Gujarati'
      },
      {
        id: 56,
        name: 'Haitian'
      },
      {
        id: 57,
        name: 'Hausa'
      },
      {
        id: 58,
        name: 'Hebrew'
      },
      {
        id: 59,
        name: 'Herero'
      },
      {
        id: 60,
        name: 'Hindi'
      },
      {
        id: 61,
        name: 'Hiri Motu'
      },
      {
        id: 62,
        name: 'Hungarian'
      },
      {
        id: 63,
        name: 'Interlingua'
      },
      {
        id: 64,
        name: 'Indonesian'
      },
      {
        id: 65,
        name: 'Interlingue'
      },
      {
        id: 66,
        name: 'Irish'
      },
      {
        id: 67,
        name: 'Igbo'
      },
      {
        id: 68,
        name: 'Inupiaq'
      },
      {
        id: 69,
        name: '  ido'
      },
      {
        id: 70,
        name: 'Icelandic'
      },
      {
        id: 71,
        name: 'Italian'
      },
      {
        id: 72,
        name: 'Inuktitut'
      },
      {
        id: 73,
        name: 'Japanese'
      },
      {
        id: 74,
        name: 'Javanese'
      },
      {
        id: 75,
        name: 'Kalaallisut'
      },
      {
        id: 76,
        name: 'Kannada'
      },
      {
        id: 77,
        name: 'Kanuri'
      },
      {
        id: 78,
        name: 'Kashmiri'
      },
      {
        id: 79,
        name: 'Kazakh'
      },
      {
        id: 80,
        name: 'Central Khmer'
      },
      {
        id: 81,
        name: 'Kikuyu'
      },
      {
        id: 82,
        name: 'Kinyarwanda'
      },
      {
        id: 83,
        name: 'Kirghiz'
      },
      {
        id: 84,
        name: 'Komi'
      },
      {
        id: 85,
        name: 'Kongo'
      },
      {
        id: 86,
        name: 'Korean'
      },
      {
        id: 87,
        name: 'Kurdish'
      },
      {
        id: 88,
        name: 'Kuanyama'
      },
      {
        id: 89,
        name: 'Latin'
      },
      {
        id: 90,
        name: 'Luxembourgish'
      },
      {
        id: 91,
        name: 'Ganda'
      },
      {
        id: 92,
        name: 'Limburgan'
      },
      {
        id: 93,
        name: 'Lingala'
      },
      {
        id: 94,
        name: 'Lao'
      },
      {
        id: 95,
        name: 'Lithuanian'
      },
      {
        id: 96,
        name: 'Luba-Katanga'
      },
      {
        id: 97,
        name: 'Latvian'
      },
      {
        id: 98,
        name: 'Manx'
      },
      {
        id: 99,
        name: 'Macedonian'
      },
      {
        id: 100,
        name: 'Malagasy'
      },
      {
        id: 101,
        name: 'Malay'
      },
      {
        id: 102,
        name: 'Malayalam'
      },
      {
        id: 103,
        name: 'Maltese'
      },
      {
        id: 104,
        name: 'Maori'
      },
      {
        id: 105,
        name: 'Marathi'
      },
      {
        id: 106,
        name: 'Marshallese'
      },
      {
        id: 107,
        name: 'Mongolian'
      },
      {
        id: 108,
        name: 'Nauru'
      },
      {
        id: 109,
        name: 'Navajo'
      },
      {
        id: 110,
        name: 'North Ndebele'
      },
      {
        id: 111,
        name: 'Nepali'
      },
      {
        id: 112,
        name: 'Ndonga'
      },
      {
        id: 113,
        name: 'Norwegian Bokmål'
      },
      {
        id: 114,
        name: 'Norwegian Nynorsk'
      },
      {
        id: 115,
        name: 'Norwegian'
      },
      {
        id: 116,
        name: 'Sichuan Yi'
      },
      {
        id: 117,
        name: 'South Ndebele'
      },
      {
        id: 118,
        name: 'Occitan'
      },
      {
        id: 119,
        name: 'Ojibwa'
      },
      {
        id: 120,
        name: 'Church Slavic'
      },
      {
        id: 121,
        name: 'Old Church Slavonic'
      },
      {
        id: 122,
        name: 'Old Bulgarian'
      },
      {
        id: 123,
        name: 'Oromo'
      },
      {
        id: 124,
        name: 'Oriya'
      },
      {
        id: 125,
        name: 'Ossetian'
      },
      {
        id: 126,
        name: 'Panjabi'
      },
      {
        id: 127,
        name: 'Pali'
      },
      {
        id: 128,
        name: 'Persian'
      },
      {
        id: 129,
        name: 'Polish'
      },
      {
        id: 130,
        name: 'Pashto'
      },
      {
        id: 131,
        name: 'Portuguese'
      },
      {
        id: 132,
        name: 'Quechua'
      },
      {
        id: 133,
        name: 'Romansh'
      },
      {
        id: 134,
        name: 'Rundi'
      },
      {
        id: 135,
        name: 'Romanian'
      },
      {
        id: 136,
        name: 'Russian'
      },
      {
        id: 137,
        name: 'Sanskrit'
      },
      {
        id: 138,
        name: 'Sardinian'
      },
      {
        id: 139,
        name: 'Sindhi'
      },
      {
        id: 140,
        name: 'Northern Sami'
      },
      {
        id: 141,
        name: 'Samoan'
      },
      {
        id: 142,
        name: 'Sango'
      },
      {
        id: 143,
        name: 'Serbian'
      },
      {
        id: 144,
        name: 'Gaelic'
      },
      {
        id: 145,
        name: 'Shona'
      },
      {
        id: 146,
        name: 'Sinhala'
      },
      {
        id: 147,
        name: 'Slovak'
      },
      {
        id: 148,
        name: 'Slovene'
      },
      {
        id: 149,
        name: 'Somali'
      },
      {
        id: 150,
        name: 'Southern Sotho'
      },
      {
        id: 151,
        name: 'Spanish'
      },
      {
        id: 152,
        name: 'Sundanese'
      },
      {
        id: 153,
        name: 'Swahili'
      },
      {
        id: 154,
        name: 'Swati'
      },
      {
        id: 155,
        name: 'Swedish'
      },
      {
        id: 156,
        name: 'Tamil'
      },
      {
        id: 157,
        name: 'Telugu'
      },
      {
        id: 158,
        name: 'Tajik'
      },
      {
        id: 159,
        name: 'Thai'
      },
      {
        id: 160,
        name: 'Tigrinya'
      },
      {
        id: 161,
        name: 'Tibetan'
      },
      {
        id: 162,
        name: 'Turkmen'
      },
      {
        id: 163,
        name: 'Tagalog'
      },
      {
        id: 164,
        name: 'Tswana'
      },
      {
        id: 165,
        name: 'Tongan'
      },
      {
        id: 166,
        name: 'Turkish'
      },
      {
        id: 167,
        name: 'Tsonga'
      },
      {
        id: 168,
        name: 'Tatar'
      },
      {
        id: 169,
        name: 'Twi'
      },
      {
        id: 170,
        name: 'Tahitian'
      },
      {
        id: 171,
        name: 'Uighur'
      },
      {
        id: 172,
        name: 'Ukrainian'
      },
      {
        id: 173,
        name: 'Urdu'
      },
      {
        id: 174,
        name: 'Uzbek'
      },
      {
        id: 175,
        name: 'Venda'
      },
      {
        id: 176,
        name: 'Vietnamese'
      },
      {
        id: 177,
        name: 'Volapük'
      },
      {
        id: 178,
        name: 'Walloon'
      },
      {
        id: 179,
        name: 'Welsh'
      },
      {
        id: 180,
        name: 'Wolof'
      },
      {
        id: 181,
        name: 'Western Frisian'
      },
      {
        id: 182,
        name: 'Xhosa'
      },
      {
        id: 183,
        name: 'Y  iddish'
      },
      {
        id: 184,
        name: 'Yoruba'
      },
      {
        id: 185,
        name: 'Zhuang'
      },
      {
        id: 186,
        name: 'Zulu'
      }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Languages', null, {});
  }
};

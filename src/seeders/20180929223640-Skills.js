'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Skills', [{
      id: 1,
      skillTypeId: 1,
      userId: 1,
      value: 'PHP',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      skillTypeId: 1,
      userId: 1,
      value: 'JavaScript',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Skills', null, {});
  }
};

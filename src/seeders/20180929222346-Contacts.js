'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Contacts', [{
      id: 1,
      contactTypeId: 1,
      userId: 1,
      value: 'Lviv, Ukraine',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      contactTypeId: 3,
      userId: 1,
      value: 'demo-user@mail.com',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Contacts', null, {});
  }
};

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('SkillTypes', [{
      id: 1,
      type: 'languages',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 2,
      type: 'frameworks',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('SkillTypes', null, {});
  }
};

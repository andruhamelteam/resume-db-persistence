'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Accounts', [{
      id: 1,
      login: 'demo-user',
      password: '$2a$04$54mnkjWhFZize.aVCvSEdeZTpdBEhJTuv8mrn5RlV.6ClZdAG8b4y',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Accounts', null, {});
  }
};

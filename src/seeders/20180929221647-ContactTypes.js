'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('ContactTypes', [{
      id: 1,
      type: 'address',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      id: 2,
      type: 'mobile phone',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 3,
      type: 'e-mail',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 4,
      type: 'skype',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      id: 5,
      type: 'linked-in',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ContactTypes', null, {});
  }
};

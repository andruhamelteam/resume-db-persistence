'use strict';

module.exports = (sequelize, DataTypes) => {
  const Education = sequelize.define('Education',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      from: {
        type: DataTypes.DATE,
        allowNull: false
      },
      to: {
        type: DataTypes.DATE
      },
      qualification: {
        type: DataTypes.STRING(100),
        allowNull: false
      },
      course: {
        type: DataTypes.STRING(100),
        allowNull: false
      }
    }
  );

  Education.associate = function (models) {
    Education.belongsTo(models.User, { foreignKey: { allowNull: false } });
    Education.belongsTo(models.EducationalOrganization, { foreignKey: { allowNull: false } });
    Education.belongsToMany(models.Resume, {through: models.ResumeEducation});

    Education.addScope('general', {
      attributes: ['id', 'from', 'to', 'qualification', 'course'],
      include: [{
        model: models.EducationalOrganization,
        attributes: ['id', 'name']
      }]
    });
  };

  return Education;
};
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Language = sequelize.define('Language',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: true
      }
    }, {
      timestamps: false
    }
  );
  Language.associate = function (models) {
    Language.belongsToMany(models.User, { through: models.UserLanguage });
    Language.belongsToMany(models.Resume, { through: models.ResumeLanguage });

    Language.addScope('general', {
      attributes: ['id', 'name']
    });
  };
  return Language;
};
'use strict';

module.exports = (sequelize, DataTypes) => {
  const ResumeEducation = sequelize.define('ResumeEducation', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ResumeEducation.associate = function(models) {
    // associations can be defined here
  };

  return ResumeEducation;
};
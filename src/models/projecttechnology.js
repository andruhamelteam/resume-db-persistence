'use strict';

module.exports = (sequelize, DataTypes) => {
  const ProjectTechnology = sequelize.define('ProjectTechnology', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ProjectTechnology.associate = function(models) {
    // associations can be defined here
  };

  return ProjectTechnology;
};
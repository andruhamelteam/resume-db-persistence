'use strict';

module.exports = (sequelize, DataTypes) => {
  const UserLanguage = sequelize.define('UserLanguage', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  UserLanguage.associate = function (models) {

  };

  return UserLanguage;
};
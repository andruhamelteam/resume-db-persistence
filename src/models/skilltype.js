'use strict';

module.exports = (sequelize, DataTypes) => {
  const SkillType = sequelize.define('SkillType',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      type: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: true
      }
    }
  );

  SkillType.associate = function (models) {
    // associations can be defined here
  };

  return SkillType;
};
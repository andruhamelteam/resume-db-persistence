'use strict';

module.exports = (sequelize, DataTypes) => {
  const ContactType = sequelize.define('ContactType',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoincrement: true
      },
      type: {
        type: DataTypes.STRING(20),
        unique: true,
        allowNull: false
      }
    }
  );

  ContactType.associate = function (models) {
    // associations can be defined here
  };

  return ContactType;
};
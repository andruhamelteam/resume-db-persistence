'use strict';

module.exports = (sequelize, DataTypes) => {
  const Responsibility = sequelize.define('Responsibility',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      value: {
        type: DataTypes.TEXT,
        allowNull: false
      }
    }
  );

  Responsibility.associate = function (models) {
    Responsibility.belongsTo(models.Experience, { foreignKey: { allowNull: false } });
  };

  return Responsibility;
};
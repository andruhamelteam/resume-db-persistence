'use strict';

module.exports = (sequelize, DataTypes) => {
  const JobPosition = sequelize.define('JobPosition', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true
    }
  }, {});

  JobPosition.associate = function (models) {
    // associations can be defined here
  };

  return JobPosition;
};
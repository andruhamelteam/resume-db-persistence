'use strict';

module.exports = (sequelize, DataTypes) => {
  const ResumeProject = sequelize.define('ResumeProject', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ResumeProject.associate = function(models) {
    // associations can be defined here
  };

  return ResumeProject;
};
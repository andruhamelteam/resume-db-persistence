'use strict';

module.exports = (sequelize, DataTypes) => {
  const ResumeSkill = sequelize.define('ResumeSkill', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ResumeSkill.associate = function(models) {
    // associations can be defined here
  };

  return ResumeSkill;
};
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Contact = sequelize.define('Contact',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      value: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: 'contact_unique_for_user'
      }
    }
  );

  Contact.associate = function (models) {
    Contact.belongsTo(models.User, { foreignKey: { allowNull: false, unique: 'contact_unique_for_user' } });
    Contact.belongsTo(models.ContactType, { foreignKey: { allowNull: false, unique: 'contact_unique_for_user' } });
    Contact.belongsToMany(models.Resume, { through: models.ResumeContact });

    Contact.addScope('general', {
      attributes: ['id', 'value'],
      include: [{
        model: models.ContactType,
        attributes: ['id', 'type']
      }]
    });
  };

  return Contact;
};
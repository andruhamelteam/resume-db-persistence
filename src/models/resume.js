'use strict';

module.exports = (sequelize, DataTypes) => {
  const Resume = sequelize.define('Resume',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: 'unique_user_resume_name'
      },
      summary: {
        type: DataTypes.TEXT
      }
    });

  Resume.associate = function (models) {
    Resume.belongsTo(
      models.User,
      {
        foreignKey: {
          allowNull: false,
          unique: 'unique_user_resume_name'
        }
      }
    );
    Resume.belongsTo(models.JobPosition, { foreignKey: { allowNull: false } });
    Resume.belongsToMany(models.Contact, { through: models.ResumeContact });
    Resume.belongsToMany(models.Skill, { through: models.ResumeSkill });
    Resume.belongsToMany(models.Experience, { through: models.ResumeExperience });
    Resume.belongsToMany(models.Project, { through: models.ResumeProject });
    Resume.belongsToMany(models.Education, { through: models.ResumeEducation });
    Resume.belongsToMany(models.Language, { through: models.ResumeLanguage });

    Resume.addScope('general', {
      attributes: ['id', 'name', 'summary'],
      include: [{
        model: models.JobPosition,
        attributes: ['id', 'name']
      }, {
        model: models.Contact.scope('general')
      }, {
        model: models.Skill,
        attributes: ['id', 'value'],
        include: [{
          model: models.SkillType,
          attributes: ['id', 'type']
        }]
      }, {
        model: models.Experience.scope('general')
      }, {
        model: models.Project.scope('general')
      }, {
        model: models.Education.scope('general')
      }, {
        model: models.Language.scope('general')
      }]
    });
  };

  return Resume;
};
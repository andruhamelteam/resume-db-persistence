'use strict';

module.exports = (sequelize, DataTypes) => {
  const EducationalOrganization = sequelize.define('EducationalOrganization', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true
    }
  }, {});

  EducationalOrganization.associate = function (models) {
    // associations can be defined here
  };

  return EducationalOrganization;
};
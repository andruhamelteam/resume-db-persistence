'use strict';

module.exports = (sequelize, DataTypes) => {
  const ResumeLanguage = sequelize.define('ResumeLanguage', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ResumeLanguage.associate = function(models) {
    // associations can be defined here
  };

  return ResumeLanguage;
};
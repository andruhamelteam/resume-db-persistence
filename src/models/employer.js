'use strict';

module.exports = (sequelize, DataTypes) => {
  const Employer = sequelize.define('Employer', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true
    }
  }, {});

  Employer.associate = function (models) {
    // associations can be defined here
  };

  return Employer;
};
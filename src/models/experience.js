'use strict';

module.exports = (sequelize, DataTypes) => {
  const Experience = sequelize.define('Experience',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      from: {
        type: DataTypes.DATE,
        allowNull: false
      },
      to: {
        type: DataTypes.DATE,
        allowNull: false
      }
    }
  );

  Experience.associate = function (models) {
    Experience.belongsTo(models.User, { foreignKey: { allowNull: false } });
    Experience.belongsTo(models.JobPosition, { foreignKey: { allowNull: false } });
    Experience.belongsTo(models.Employer, { foreignKey: { allowNull: false } });
    Experience.hasMany(models.Responsibility, { foreignKey: { allowNull: false } });

    Experience.belongsToMany(models.Resume, {through: models.ResumeExperience});

    Experience.addScope('general', {
      attributes: ['id', 'from', 'to'],
      include: [{
        model: models.JobPosition,
        attributes: ['id', 'name']
      }, {
        model: models.Employer,
        attributes: ['id', 'name']
      }, {
        model: models.Responsibility,
        attributes: ['id', 'value']
      }]
    });
  };

  return Experience;
};
'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      firstName: {
        type: DataTypes.STRING(50),
        allowNull: false
      },
      lastName: {
        type: DataTypes.STRING(50),
        allowNull: false
      }
    }
  );

  User.associate = function (models) {
    User.belongsTo(models.Account, { foreignKey: { allowNull: false } })
    User.hasMany(models.Contact, { foreignKey: { allowNull: false } });
    User.hasMany(models.Education, { foreignKey: { allowNull: false } });
    User.hasMany(models.Experience, { foreignKey: { allowNull: false } });
    User.hasMany(models.Project, { foreignKey: { allowNull: false } });
    User.hasMany(models.Resume, { foreignKey: { allowNull: false } });
    User.hasMany(models.Skill, { foreignKey: { allowNull: false } });

    User.belongsToMany(models.Language, { through: models.UserLanguage });

    User.addScope('general', (userLogin) => {
      return {
        attributes: ['id', 'firstName', 'lastName'],
        include: [{
          model: models.Account,
          attributes: ['login'],
          where: {
            login: userLogin
          }
        }]
      }
    });

    User.addScope('contacts', {
      include: [{
        model: models.Contact.scope('general')
      }]
    });

    User.addScope('skills', {
      include: [{
        model: models.Skill.scope('general')
      }]
    });

    User.addScope('experience', {
      include: [{
        model: models.Experience.scope('general')
      }]
    });

    User.addScope('education', {
      include: [{
        model: models.Education.scope('general')
      }]
    });

    User.addScope('projects', {
      include: models.Project.scope('general')
    })
  };

  return User;
};
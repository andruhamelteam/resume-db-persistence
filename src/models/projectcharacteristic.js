'use strict';

module.exports = (sequelize, DataTypes) => {
  const ProjectCharacteristic = sequelize.define('ProjectCharacteristic',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      value: {
        type: DataTypes.TEXT,
        allowNull: false
      }
    }
  );

  ProjectCharacteristic.associate = function (models) {
    ProjectCharacteristic.belongsTo(models.Project, { foreignKey: { allowNull: false } });
  };

  return ProjectCharacteristic;
};
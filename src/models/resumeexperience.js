'use strict';

module.exports = (sequelize, DataTypes) => {
  const ResumeExperience = sequelize.define('ResumeExperience', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ResumeExperience.associate = function(models) {
    // associations can be defined here
  };

  return ResumeExperience;
};
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      login: {
        type: DataTypes.STRING(100),
        unique: true,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING(100),
        allowNull: false
      }
    }
  );
  Account.associate = function (models) {
    Account.hasOne(models.User, { foreignKey: { allowNull: false } });
  };

  return Account;
};
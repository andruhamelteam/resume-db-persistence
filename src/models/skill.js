'use strict';

module.exports = (sequelize, DataTypes) => {
  const Skill = sequelize.define('Skill',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      value: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: 'unique-skill-for-user'
      }
    }
  );

  Skill.associate = function (models) {
    Skill.belongsTo(models.User, {
      foreignKey: {
        allowNull: false,
        unique: 'unique-skill-for-user'
      }
    });
    Skill.belongsTo(models.SkillType, {
      foreignKey: {
        allowNull: false,
        unique: 'unique-skill-for-user'
      }
    });
    Skill.belongsToMany(models.Resume, { through: models.ResumeSkill });

    Skill.addScope('general', {
      attributes: ['id', 'value'],
      include: [{
        model: models.SkillType,
        attributes: ['id', 'type']
      }]
    });
  };

  return Skill;
};
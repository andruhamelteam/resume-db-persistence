'use strict';

module.exports = (sequelize, DataTypes) => {
  const Technology = sequelize.define('Technology',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false
      }
    }
  );

  Technology.associate = function (models) {
    Technology.belongsToMany(models.Project, { through: models.ProjectTechnology });
  };

  return Technology;
};
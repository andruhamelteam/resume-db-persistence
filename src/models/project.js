'use strict';

module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      name: {
        type: DataTypes.STRING(100),
        allowNull: false,
        unique: 'user_project_unique'
      }
    }
  );

  Project.associate = function (models) {
    Project.belongsTo(
      models.User,
      {
        foreignKey: {
          allowNull: false,
          unique: 'user_project_unique'
        }
      }
    );

    Project.hasMany(models.ProjectCharacteristic, { foreignKey: { allowNull: false } });
    Project.belongsToMany(models.Technology, { through: models.ProjectTechnology });
    Project.belongsToMany(models.Resume, {through: models.ResumeProject});

    Project.addScope('general', {
      attributes: ['id', 'name'],
      include: [{
        model: models.ProjectCharacteristic,
        attributes: ['id', 'value']
      }, {
        model: models.Technology,
        attributes: ['id', 'name']
      }]
    });
  };

  return Project;
};
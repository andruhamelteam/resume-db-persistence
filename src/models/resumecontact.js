'use strict';

module.exports = (sequelize, DataTypes) => {
  const ResumeContact = sequelize.define('ResumeContact', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    }
  }, {});

  ResumeContact.associate = function(models) {
    // associations can be defined here
  };

  return ResumeContact;
};
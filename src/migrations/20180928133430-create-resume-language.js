'use strict';

let { ResumeLanguage: model } = require(__dirname + '/../models/index.js');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      model.tableName,
      model.attributes,
      model.options
    );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable(model.tableName);
  }
};